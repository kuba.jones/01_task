const http = require('http');
const url = require('url');
const redis = require('redis');
const async = require('async');

const parsing = require('./my_modules/parsing.js');
const files = require('./my_modules/files.js');

const httpPort = 3000;
const redisPort = process.env.REDIS_PORT || 6379;
const redisHost = process.env.REDIS_ADDR || '172.17.0.2';
const jsonRecordsFile = 'queries.txt';

var server = http.createServer();
var redisClient = redis.createClient(redisPort, redisHost);

function increaseCountQuery(obj, cb) {
	if(!isNaN(obj.count)) {
		redisClient.incrby('count', obj.count, (err, reply) => {
				console.log(reply); // shows current counter
				cb(err);
			});
	}
}
function parallel(queryRecord, cb) {
	async.parallel([
		(cb) => {
			files.appendObjectToJson(queryRecord, jsonRecordsFile, (done) => {
				return cb(null, done);
			});
		},
		(cb) => {
			increaseCountQuery(queryRecord.query, (done) => {
				return cb(null, done);
			})
		}
		], cb);
}
server.on('request', (req, res) => {
	var urlParsed = url.parse(req.url);
	if(req.method === 'GET' && urlParsed.pathname === '/track'){
		var queryRecord = parsing.parseQuery(urlParsed.query);
		parallel(queryRecord, (err, done) => {
			if (err) throw err;
			if (done[0] || done[1]){
				res.statusCode = 500;
			} else {
				res.statusCode = 200;
			}
			res.end();
		});
	} else {
		res.statusCode = 404;
		res.end();
	}
}).on('error', (err) => {
	// handle errors when creating server, eg. port already binded
	console.log('cannot create http server - exiting');
	server.quit();
}).listen(httpPort);

redisClient.on('connect', () => {
	console.log('Connected to redis!');
	if(redisClient.exists('count', (err, reply) => {
		if (reply !== 1) {
				console.log('FYI: count key doesnt exist yet');
		}
	}));
}).on('error', (err) => {
	console.log(err);
});