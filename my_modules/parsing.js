const querystring = require('querystring');

var Query = function(data) {
	this.date = new Date();
	this.query = querystring.parse(data)
}

exports.parseQuery = function(query) {
	var queryRecord = new Query(query);
	return queryRecord;	
}