const expect = require('chai').expect;
var task1 = require("../my_modules/parsing.js");

describe("Test query parsing function", () => {
	it("converts url to query object", () => {
		var query1 = "foo=2&bar=1&count=4";
		var url1 = task1.parseQuery(query1);

		expect(url1.query).to.eql({ "foo":"2","bar":"1","count":"4" });
	});
});
