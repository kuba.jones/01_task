const expect = require('chai').expect;
const request = require('request');

describe("Http request checking", () => {
	var url1 = "http://localhost:3000/track"
	var url2 = "http://localhost:3000/wrongOne"

	it("Test /track route that should return 200", (done) => {
		request(url1, (err, res, body) => {
			expect(res.statusCode).to.eql(200);
			done();
		});
	});
	it("Test route that should return 404", (done) => {
		request(url2, (err, res, body) => {
			expect(res.statusCode).to.eql(404);
			done();
		});
	});
});
