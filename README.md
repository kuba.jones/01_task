Task 1 node app
=========================

Basic commands
-------------------------
- npm update - downloads dependencies
- npm start - starts app
- npm test - runs tests (requires app to be started for http request testing)


Redis commands
-------------------------
- npm run redis-run - creates redis instance to docker container (requires docker to be installed)
- npm run redis-start - starts existing redis container
- npm run redis-stop - stops existing redis container

or 

you can start app with your own redis instance using env/shell variables like this - REDIS_PORT=6379 REDIS_ADDR=127.0.0.1 npm start

Changelog
-------------------------
3.6.2016
- dependencies for test moved to devDependencies in package.json
- object prototypes are in use for parsing queries
- writing to file and redis is now run async parallely and functions are returning status in callback, after both done statusCode is send according to success of processess
- functions are returning status in callback
- queries are now appended to file, json is not valid anymore
- if http server creation fails, err is thrown and app will quit
