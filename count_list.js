const redis = require('redis');

const redisPort = 6379;
const redisHost = '172.17.0.2';

var redisClient = redis.createClient(redisPort, redisHost);

redisClient.on('connect', () => {
	console.log('Connected to redis!');
	redisClient.get('count', (err, reply) => {
		if (reply) {
			console.log('count: ',reply);
			redisClient.quit();
		} else {
			console.log('count key doesnt exist');
			redisClient.quit();
		}
	});
}).on('error', (err) => {
	console.log(err);
});